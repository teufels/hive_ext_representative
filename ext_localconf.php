<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtRepresentative',
            'Hiveextrepresentativecontactfilter',
            [
                'Contact' => 'filter'
            ],
            // non-cacheable actions
            [
                'Contact' => 'filter'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextrepresentativecontactfilter {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_representative') . 'Resources/Public/Icons/user_plugin_hiveextrepresentativecontactfilter.svg
                        title = LLL:EXT:hive_ext_representative/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_representative_domain_model_hiveextrepresentativecontactfilter
                        description = LLL:EXT:hive_ext_representative/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_representative_domain_model_hiveextrepresentativecontactfilter.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextrepresentative_hiveextrepresentativecontactfilter
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function($extKey, $globals)
    {
        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hiveextrepresentativecontactfilter >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveextrepresentativecontactfilter {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Representative (filter)
                            description = Filters representatives for country and zips
                            tt_content_defValues {
                                CType = list
                                list_type = hiveextrepresentative_hiveextrepresentativecontactfilter
                            }
                        }
                        show := addToList(hiveextrepresentativecontactfilter)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
//        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
//            \HIVE\HiveExtEvent\Hooks\PageLayoutView\EventListPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);