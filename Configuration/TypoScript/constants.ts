
plugin.tx_hiveextrepresentative_hiveextrepresentativecontactfilter {
    view {
        # cat=plugin.tx_hiveextrepresentative_hiveextrepresentativecontactfilter/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_representative/Resources/Private/Templates/
        # cat=plugin.tx_hiveextrepresentative_hiveextrepresentativecontactfilter/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_representative/Resources/Private/Partials/
        # cat=plugin.tx_hiveextrepresentative_hiveextrepresentativecontactfilter/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_representative/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextrepresentative_hiveextrepresentativecontactfilter//a; type=string; label=Default storage PID
        storagePid =
    }
}
