<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtRepresentative',
            'Hiveextrepresentativecontactfilter',
            'hive_ext_representative :: Contact :: filter'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_representative', 'Configuration/TypoScript', 'hive_ext_representative');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextrepresentative_domain_model_contact', 'EXT:hive_ext_representative/Resources/Private/Language/locallang_csh_tx_hiveextrepresentative_domain_model_contact.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextrepresentative_domain_model_contact');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextrepresentative_domain_model_country', 'EXT:hive_ext_representative/Resources/Private/Language/locallang_csh_tx_hiveextrepresentative_domain_model_country.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextrepresentative_domain_model_country');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextrepresentative_domain_model_ziprange', 'EXT:hive_ext_representative/Resources/Private/Language/locallang_csh_tx_hiveextrepresentative_domain_model_ziprange.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextrepresentative_domain_model_ziprange');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextrepresentative_domain_model_countryzip', 'EXT:hive_ext_representative/Resources/Private/Language/locallang_csh_tx_hiveextrepresentative_domain_model_countryzip.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextrepresentative_domain_model_countryzip');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_representative',
            'tx_hiveextrepresentative_domain_model_contact'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_representative',
            'tx_hiveextrepresentative_domain_model_country'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_representative',
            'tx_hiveextrepresentative_domain_model_ziprange'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_representative',
            'tx_hiveextrepresentative_domain_model_countryzip'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder