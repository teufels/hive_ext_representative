<?php
namespace HIVE\HiveExtRepresentative\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Contact
 */
class Contact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * firstname
     *
     * @var string
     */
    protected $firstname = '';

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * mail
     *
     * @var string
     */
    protected $mail = '';

    /**
     * image
     *
     * @var string
     */
    protected $image = '';

    /**
     * countryZip
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtRepresentative\Domain\Model\CountryZip>
     */
    protected $countryZip = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->countryZip = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     *
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     * @return void
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the mail
     *
     * @return string $mail
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Sets the mail
     *
     * @param string $mail
     * @return void
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Returns the image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param string $image
     * @return void
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Adds a CountryZip
     *
     * @param \HIVE\HiveExtRepresentative\Domain\Model\CountryZip $countryZip
     * @return void
     */
    public function addCountryZip(\HIVE\HiveExtRepresentative\Domain\Model\CountryZip $countryZip)
    {
        $this->countryZip->attach($countryZip);
    }

    /**
     * Removes a CountryZip
     *
     * @param \HIVE\HiveExtRepresentative\Domain\Model\CountryZip $countryZipToRemove The CountryZip to be removed
     * @return void
     */
    public function removeCountryZip(\HIVE\HiveExtRepresentative\Domain\Model\CountryZip $countryZipToRemove)
    {
        $this->countryZip->detach($countryZipToRemove);
    }

    /**
     * Returns the countryZip
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtRepresentative\Domain\Model\CountryZip> $countryZip
     */
    public function getCountryZip()
    {
        return $this->countryZip;
    }

    /**
     * Sets the countryZip
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtRepresentative\Domain\Model\CountryZip> $countryZip
     * @return void
     */
    public function setCountryZip(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $countryZip)
    {
        $this->countryZip = $countryZip;
    }
}
