<?php
namespace HIVE\HiveExtRepresentative\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Country
 */
class Country extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * iso
     *
     * @var string
     */
    protected $iso = '';

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the iso
     *
     * @return string $iso
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * Sets the iso
     *
     * @param string $iso
     * @return void
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
    }
}
