<?php
namespace HIVE\HiveExtRepresentative\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * ZipRange
 */
class ZipRange extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * area
     *
     * @var string
     */
    protected $area = '';

    /**
     * Returns the area
     *
     * @return string $area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Sets the area
     *
     * @param string $area
     * @return void
     */
    public function setArea($area)
    {
        $this->area = $area;
    }
}
