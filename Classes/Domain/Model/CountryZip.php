<?php
namespace HIVE\HiveExtRepresentative\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * CountryZip
 */
class CountryZip extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * country
     *
     * @var \HIVE\HiveExtRepresentative\Domain\Model\Country
     */
    protected $country = null;

    /**
     * zipRange
     *
     * @var \HIVE\HiveExtRepresentative\Domain\Model\ZipRange
     */
    protected $zipRange = null;

    /**
     * Returns the country
     *
     * @return \HIVE\HiveExtRepresentative\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param \HIVE\HiveExtRepresentative\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\HIVE\HiveExtRepresentative\Domain\Model\Country $country)
    {
        $this->country = $country;
    }

    /**
     * Returns the zipRange
     *
     * @return \HIVE\HiveExtRepresentative\Domain\Model\ZipRange $zipRange
     */
    public function getZipRange()
    {
        return $this->zipRange;
    }

    /**
     * Sets the zipRange
     *
     * @param \HIVE\HiveExtRepresentative\Domain\Model\ZipRange $zipRange
     * @return void
     */
    public function setZipRange(\HIVE\HiveExtRepresentative\Domain\Model\ZipRange $zipRange)
    {
        $this->zipRange = $zipRange;
    }
}
