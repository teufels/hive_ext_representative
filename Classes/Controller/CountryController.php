<?php
namespace HIVE\HiveExtRepresentative\Controller;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * CountryController
 */
class CountryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * countryRepository
     *
     * @var \HIVE\HiveExtRepresentative\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $countries = $this->countryRepository->findAll();
        $this->view->assign('countries', $countries);
    }
}
