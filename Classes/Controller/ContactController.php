<?php
namespace HIVE\HiveExtRepresentative\Controller;

/***
 *
 * This file is part of the "hive_ext_representative" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * ContactController
 */
class ContactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * contactRepository
     *
     * @var \HIVE\HiveExtRepresentative\Domain\Repository\ContactRepository
     * @inject
     */
    protected $contactRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $contacts = $this->contactRepository->findAll();
        $this->view->assign('contacts', $contacts);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtRepresentative\Domain\Model\Contact $contact
     * @return void
     */
    public function showAction(\HIVE\HiveExtRepresentative\Domain\Model\Contact $contact)
    {
        $this->view->assign('contact', $contact);
    }

    /**
     * action filter
     *
     * @return void
     */
    public function filterAction()
    {

    }
}
