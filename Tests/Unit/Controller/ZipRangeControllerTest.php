<?php
namespace HIVE\HiveExtRepresentative\Tests\Unit\Controller;

/**
 * Test case.
 */
class ZipRangeControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtRepresentative\Controller\ZipRangeController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HIVE\HiveExtRepresentative\Controller\ZipRangeController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllZipRangesFromRepositoryAndAssignsThemToView()
    {

        $allZipRanges = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $zipRangeRepository = $this->getMockBuilder(\HIVE\HiveExtRepresentative\Domain\Repository\ZipRangeRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $zipRangeRepository->expects(self::once())->method('findAll')->will(self::returnValue($allZipRanges));
        $this->inject($this->subject, 'zipRangeRepository', $zipRangeRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('zipRanges', $allZipRanges);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
