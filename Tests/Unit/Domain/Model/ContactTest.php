<?php
namespace HIVE\HiveExtRepresentative\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ContactTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtRepresentative\Domain\Model\Contact
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtRepresentative\Domain\Model\Contact();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFirstnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFirstname()
        );
    }

    /**
     * @test
     */
    public function setFirstnameForStringSetsFirstname()
    {
        $this->subject->setFirstname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'firstname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLastnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLastname()
        );
    }

    /**
     * @test
     */
    public function setLastnameForStringSetsLastname()
    {
        $this->subject->setLastname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lastname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone()
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'phone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMail()
        );
    }

    /**
     * @test
     */
    public function setMailForStringSetsMail()
    {
        $this->subject->setMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForStringSetsImage()
    {
        $this->subject->setImage('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCountryZipReturnsInitialValueForCountryZip()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCountryZip()
        );
    }

    /**
     * @test
     */
    public function setCountryZipForObjectStorageContainingCountryZipSetsCountryZip()
    {
        $countryZip = new \HIVE\HiveExtRepresentative\Domain\Model\CountryZip();
        $objectStorageHoldingExactlyOneCountryZip = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCountryZip->attach($countryZip);
        $this->subject->setCountryZip($objectStorageHoldingExactlyOneCountryZip);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneCountryZip,
            'countryZip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addCountryZipToObjectStorageHoldingCountryZip()
    {
        $countryZip = new \HIVE\HiveExtRepresentative\Domain\Model\CountryZip();
        $countryZipObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryZipObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($countryZip));
        $this->inject($this->subject, 'countryZip', $countryZipObjectStorageMock);

        $this->subject->addCountryZip($countryZip);
    }

    /**
     * @test
     */
    public function removeCountryZipFromObjectStorageHoldingCountryZip()
    {
        $countryZip = new \HIVE\HiveExtRepresentative\Domain\Model\CountryZip();
        $countryZipObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryZipObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($countryZip));
        $this->inject($this->subject, 'countryZip', $countryZipObjectStorageMock);

        $this->subject->removeCountryZip($countryZip);
    }
}
