<?php
namespace HIVE\HiveExtRepresentative\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ZipRangeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtRepresentative\Domain\Model\ZipRange
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtRepresentative\Domain\Model\ZipRange();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getAreaReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getArea()
        );
    }

    /**
     * @test
     */
    public function setAreaForStringSetsArea()
    {
        $this->subject->setArea('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'area',
            $this->subject
        );
    }
}
