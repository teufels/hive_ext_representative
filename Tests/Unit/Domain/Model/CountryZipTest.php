<?php
namespace HIVE\HiveExtRepresentative\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class CountryZipTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtRepresentative\Domain\Model\CountryZip
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtRepresentative\Domain\Model\CountryZip();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForCountry()
    {
        self::assertEquals(
            null,
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForCountrySetsCountry()
    {
        $countryFixture = new \HIVE\HiveExtRepresentative\Domain\Model\Country();
        $this->subject->setCountry($countryFixture);

        self::assertAttributeEquals(
            $countryFixture,
            'country',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipRangeReturnsInitialValueForZipRange()
    {
        self::assertEquals(
            null,
            $this->subject->getZipRange()
        );
    }

    /**
     * @test
     */
    public function setZipRangeForZipRangeSetsZipRange()
    {
        $zipRangeFixture = new \HIVE\HiveExtRepresentative\Domain\Model\ZipRange();
        $this->subject->setZipRange($zipRangeFixture);

        self::assertAttributeEquals(
            $zipRangeFixture,
            'zipRange',
            $this->subject
        );
    }
}
