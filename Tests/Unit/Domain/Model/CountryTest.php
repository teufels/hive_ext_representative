<?php
namespace HIVE\HiveExtRepresentative\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class CountryTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtRepresentative\Domain\Model\Country
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtRepresentative\Domain\Model\Country();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIsoReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIso()
        );
    }

    /**
     * @test
     */
    public function setIsoForStringSetsIso()
    {
        $this->subject->setIso('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iso',
            $this->subject
        );
    }
}
